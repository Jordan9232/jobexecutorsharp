using System;

namespace JobExecutorSharp
{
  public class JobResult<TResult>
  {
    public Type ReturnType { get; set; }
    public TResult Value { get; set; }

    public JobResult(Type returnTypeP, TResult valueP)
    {
      ReturnType = returnTypeP;
      Value = valueP;
    }
  }
}
