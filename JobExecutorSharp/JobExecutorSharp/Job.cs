using System;

namespace JobExecutorSharp
{
  public class Job : JobBase<object>, IJob<object>
  {
    public Job()
    {
      Build(() => { });
    }

    #region Job Action Builders
    public void Build(
      Action delegateP)
    {
      BuildWorkload(delegateP);
    }

    public void Build<T>(
      Action<T> delegateP,
      T p1)
    {
      BuildWorkload(delegateP, p1);
    }

    public void Build<T1, T2>(
      Action<T1, T2> delegateP,
      T1 p1, T2 p2)
    {
      BuildWorkload(delegateP, p1, p2);
    }

    public void Build<T1, T2, T3>(
      Action<T1, T2, T3> delegateP,
      T1 p1, T2 p2, T3 p3)
    {
      BuildWorkload(delegateP, p1, p2, p3);
    }

    public void Build<T1, T2, T3, T4>(
      Action<T1, T2, T3, T4> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4);
    }

    public void Build<T1, T2, T3, T4, T5>(
      Action<T1, T2, T3, T4, T5> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5);
    }

    public void Build<T1, T2, T3, T4, T5, T6>(
      Action<T1, T2, T3, T4, T5, T6> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7>(
      Action<T1, T2, T3, T4, T5, T6, T7> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(
      Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16);
    }
    #endregion
  }

  public class Job<TResult> : JobBase<TResult>, IJob<TResult>
  {
    public Job()
    {
      Build(() => default);
    }

    #region Job Func Builders
    public void Build(
      Func<TResult> delegateP)
    {
      BuildWorkload(delegateP);
    }

    public void Build<T>(
      Func<T, TResult> delegateP,
      T p1)
    {
      BuildWorkload(delegateP, p1);
    }

    public void Build<T1, T2>(
      Func<T1, T2, TResult> delegateP,
      T1 p1, T2 p2)
    {
      BuildWorkload(delegateP, p1, p2);
    }

    public void Build<T1, T2, T3>(
      Func<T1, T2, T3, TResult> delegateP,
      T1 p1, T2 p2, T3 p3)
    {
      BuildWorkload(delegateP, p1, p2, p3);
    }

    public void Build<T1, T2, T3, T4>(
      Func<T1, T2, T3, T4, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4);
    }

    public void Build<T1, T2, T3, T4, T5>(
      Func<T1, T2, T3, T4, T5, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5);
    }

    public void Build<T1, T2, T3, T4, T5, T6>(
      Func<T1, T2, T3, T4, T5, T6, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7>(
      Func<T1, T2, T3, T4, T5, T6, T7, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15);
    }

    public void Build<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(
      Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, TResult> delegateP,
      T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16)
    {
      BuildWorkload(delegateP, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16);
    }
    #endregion
  }
}
