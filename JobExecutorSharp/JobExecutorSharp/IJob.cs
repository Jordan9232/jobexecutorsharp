using System;

namespace JobExecutorSharp
{
  public interface IJob<TResult>
  {
    Delegate Function { get; }
    object[] Arguments { get; }

    JobResult<TResult> Execute();
    object GetArguement(int positionP);
    T GetArgument<T>(int positionP);
    void SetArguement<T>(int positionP, T valueP);
  }
}
