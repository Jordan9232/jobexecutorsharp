using System;

namespace JobExecutorSharp
{
  public abstract class JobBase<TResult> : IJob<TResult>
  {
    public Delegate Function { get; protected set; }
    public object[] Arguments { get; protected set; }

    public JobBase()
    {
      Function = (Func<object>)(() => null);
      Arguments = new object[] { };
    }

    public JobResult<TResult> Execute()
    {
      if (Function.Method.ReturnType.Equals(typeof(void)))
      {
        Function.DynamicInvoke(Arguments);

        return null;
      }

      return new JobResult<TResult>(typeof(TResult), (TResult)Function.DynamicInvoke(Arguments));
    }

    public object GetArguement(int positionP)
    {
      return Arguments[positionP];
    }

    public T GetArgument<T>(int positionP)
    {
      return (T)Arguments[positionP];
    }

    public void SetArguement<T>(int positionP, T valueP)
    {
      ValidateArguement(positionP, valueP);

      Arguments[positionP] = valueP;
    }

    protected void BuildWorkload(Delegate delegateP, params object[] paramsP)
    {
      Function = delegateP;
      Arguments = paramsP;
    }

    private void ValidateArguement<T>(int positionP, T valueP)
    {
      var function_parameters = Function.Method.GetParameters();
      var parameter_type = function_parameters[positionP].ParameterType;
      var arguement_type = valueP.GetType();

      if (!(arguement_type.Equals(parameter_type) || arguement_type.IsSubclassOf(parameter_type)))
      {
        throw new ArgumentException($"Object of type '{arguement_type.ToString()}' cannot be converted to type '{parameter_type.ToString()}'");
      }
    }
  }
}
