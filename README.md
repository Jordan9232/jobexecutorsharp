# JobExecutorSharp

## Purpose

A simple library that uses a wrapper class for executing Delegate functions. 
This makes developing easier when using Action and Func classes. Instead of 
using just a Delegate class which may be hard to identify a return type (if any) 
and/or parameters (if any), and troubles with acting invocation, you can use 
this library instead.

On its own, if you know what your delegate function is, its parameters, and 
return type, you could just as easily use Delegate and invoke it easily, but 
this is especially useful for when you are creating your own functions that 
require a Delegate as a parameter and have to handle its execution. Instead, 
you can just have your function require a Job object instead, and your function 
will execute the job with ease instead of you trying to determine how to execute 
it.

## Why not just use Delegate?

The reason this wrapper class is useful is because the initialization of this 
wrapper class is much more distinguishable than the Delegate class is and also 
provides a very simple way to execute the delegate function without having to 
worry so much about typical runtime errors when trying to execute a delegate 
normally, such as using providing the correct number of parameters the delegate 
requires, using correct parameter types, and capturing the appropriate return 
type. A wrapper object can either be instantiated with no generic type or can 
take a single generic type. An instance with no generic type means that the 
delegate function in the wrapper is void (Action object), whereas an instance 
with a generic type has a return type (Func object).

With that in mind, this means that the wrapper class with no generic type can 
only be created using Action objects, whereas with a generic type can only be
created using Func objects.

## Delegate Initialization

Okay so, no generic type means a void function and using a generic type means
the function has a return type, why do we still need this library? The library
is still useful because when using Action and Func objects on their own you may
have to specify up to 17 generic types for a SINGLE object. 

An Action object can have up to 16 generic types (`Action<T1, T2, T3..., T15, T16>`), 
one for each parameter of the function and a Func object can have up to 17 
generic types (`Func<T1, T2, T3..., T15, T16, TResult>`), one for each parameter 
of the function and the last type for the return type of the function. This 
library makes initialization so much simpler because there is no need to specify 
so many potential generic types. Instead, a simple lamda expression is all you 
need to you need to initialize the wrapper class and it will automatically detect 
the signature of the method. 

The wrapper class with no generic type will detect any variation of the Action 
class, whereas with a generic type will detect any variation of the Func class 
without the need to be die-hard specific.

## Wrapper Initialization

The wrapper class only uses a default constructor and the delegate function is
created using the "Build" function which requires a variation of the Action of 
Func objects and values for the objects's parameters. There is a different method 
signature for each variation of the Action and Func objects that make the 
auto-delegate detection possible.

**`Job` Build Functions:**
```
Build(Action delegateP)

Build<T>(Action<T> delegateP, T p1)

Build<T1, T2>(Action<T1,T2) delegateP, T1 p1, T2 p2)

and so on...
Build<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16>(Action<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16> delegateP, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16)
```

**`Job<TResult>` Build Functions:**
```
Build(Func<TResult> delegateP)

Build<T>(Func<T,TResult> delegateP, T p1)

Build<T1,T2>(Func<T1,T2,TResult> delegateP, T1 p1, T2 p2)

and so on...
Build<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,TResult>(Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,T13,T14,T15,T16,TResult> delegateP, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16)
```

Execution of the delegate using its parameters is not done until the `Execute()`
function is called on the Job object which will always return a `JobResult` object
which contains properties for the `Type` of object that was returned and the value
the delegate function returned as a `object`. In the case of the delegate function
being a void function, the `JobResult` will always be `null`.

## Examples

**`Job`:**

Simple void function with no parameters:
```
var job = new Job();
job.Build(() => 
{
  Console.WriteLine("Test logger using job class");
});

var result = job.Execute(); // result will be null because it's a void function
```

Simple void function with string parameter:
```
var job = new Job();
job.Build((messageP) => 
{
  Console.WriteLine(messageP);
}, "Test console log with parameter");

job.Execute();
```

**`Job<TResult>`:**

Simple function returning int with no parameters:
```
var job = new Job<int>();
job.Build(() => 
{
  Console.WriteLine("Test int return");

  return 3;
});

var result = job.Execute(); // result will be JobResult with Type = int and Value = 3
```

Simple function returning string with 3 string parameters:
```
var job = new Job<string>();
job.Build((stringValue, search, replacement) =>
{
  Console.WriteLine("Test string return with string parameters");

  return stringValue.Replace(search, replacement);
}, "This text contains the word 'apple'", "apple", "banana");

var result = job.Execute(); // result will be JobResult with Type = string and Value = "This text contains the word 'banana'"
```